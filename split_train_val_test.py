import os
import requests
import numpy as np
import multiprocessing
from io import BytesIO
from PIL import Image, ImageOps
from sql_classes import Session, Product
from sqlalchemy import func, desc, and_
from typing import List

NUMBER_OF_CLASS = 10
TRAIN_SIZE = 0.6
VAL_SIZE = 0.2
TEST_SIZE = 0.2

dataset_folder = './dataset'


def save_prod(prods: List[Product], label: str, train_val_test: str):
    output_folder = os.path.join(dataset_folder, train_val_test, label)
    os.makedirs(output_folder, exist_ok=True)
    for prod in prods:
        response = requests.get(prod.image_url)

        img = Image.open(BytesIO(response.content))

        max_dim = max(img.size)
        border = (int((max_dim - img.size[0]) / 2),
                  int((max_dim - img.size[1]) / 2))

        img = ImageOps.expand(img, border=border, fill='white')
        img = ImageOps.fit(img, (max_dim, max_dim))
        img.save(os.path.join(output_folder, (prod.codice_amazon + '.jpg')))
    print('{label} - {state} done'.format(label=label, state=train_val_test))


def split_product_dataset(prod_list: List[Product], label: str):
    np.random.shuffle(prod_list)
    prod_len = len(prod_list)
    train_len = int(prod_len * TRAIN_SIZE)
    train_prod = prod_list[:train_len]
    train_folder = os.path.join(dataset_folder, 'train', label)

    val_len = int(prod_len * VAL_SIZE)
    val_prod = prod_list[train_len: train_len + val_len]
    val_folder = os.path.join(dataset_folder, 'val', label)

    test_prod = prod_list[train_len + val_len:]
    test_folder = os.path.join(dataset_folder, 'test', label)

    os.makedirs(train_folder, exist_ok=True)
    os.makedirs(val_folder, exist_ok=True)
    os.makedirs(test_folder, exist_ok=True)

    print('saving {n_picture} picture for {label}'.format(n_picture=prod_len, label=label))
    list_prod = [train_prod, val_prod, test_prod]
    list_label = [label, label, label]
    list_dest = ['train', 'val', 'test']
    for prod, label, dest in zip(list_prod, list_label, list_dest):
        save_prod(prod, label, dest)


def main():
    s = Session()
    # class_count = List[(str(label), int(count_label))]
    class_count = s.query(Product.product_type_name, func.count(Product.product_type_name)). \
        filter(and_(Product.image_url != None, Product.product_type_name != None)). \
        group_by(Product.product_type_name). \
        order_by(desc(func.count(Product.product_type_name))). \
        limit(NUMBER_OF_CLASS). \
        all()
    s.close()

    labels = []
    dataset = {}
    s = Session()
    for element in class_count:
        labels.append(element[0])
        dataset[element[0]] = s.query(Product). \
            filter(and_(Product.image_url != None,
                        Product.product_type_name != None,
                        Product.product_type_name == element[0])). \
            all()
        # print('{count} - {label}'.format(label=element[0], count=element[1]))

    s.close()

    # mutiprocessing
    jobs = []
    for key in dataset.keys():
        p = multiprocessing.Process(target=split_product_dataset, args=(dataset[key], key))
        jobs.append(p)
        p.start()


if __name__ == '__main__':
    main()
