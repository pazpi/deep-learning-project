import requests
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont
from sqlalchemy import and_

from sql_classes import Product, Session


def main():
    # sessione database
    session = Session()
    # ottengo tutti i prodotti con link e label
    all_prod = session.query(Product).filter(and_(Product.image_url != None, Product.product_type_name != None)).all()
    n_prod = session.query(Product).count()
    session.close()

    for counter, prod in enumerate(all_prod, 1):
        response = requests.get(prod.image_url)
        img = Image.open(BytesIO(response.content))
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype('/usr/share/fonts/truetype/DroidSans.ttf', 32)
        draw.text((10, 10), prod.product_type_name, (0, 0, 0), font=font)
        img.save('images/' + prod.codice_amazon + '.jpg')
        print("Processed product {} ({} of {})".format(prod.codice_amazon, counter, n_prod))


if __name__ == "__main__":
    main()
