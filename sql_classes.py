from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Query

Base = declarative_base()
# DEBUG: aggiungere echo=True come parametro di engine per vedere le richieste SQL
engine = create_engine('sqlite:///databases/database.sqlite')
Session = sessionmaker(bind=engine)


class Product(Base):
    __tablename__ = 'products'
    id = Column(Integer, primary_key=True)
    codice_amazon = Column(String)
    name = Column(String)
    image_url = Column(String)
    product_type_name = Column(String)

    def __init__(self, codice_amazon, name, image_url, product_type_name):
        self.codice_amazon = codice_amazon
        self.name = name
        self.image_url = image_url
        self.product_type_name = product_type_name

    def __repr__(self):
        template_str = "<Product(id={}," \
                       "codice_amazon='{}'," \
                       "name='{}'," \
                       "image_url='{}'," \
                       "product_type_name='{}')>".format(self.id,
                                                         self.codice_amazon,
                                                         self.name,
                                                         self.image_url,
                                                         self.product_type_name)
        return template_str

    @classmethod
    def query_by_id(cls, session: Session, prod_id) -> Query:
        return session.query(cls).filter(cls.id == prod_id)

    @classmethod
    def query_by_codice_amazon(cls, session: Session, codice_amazon) -> Query:
        return session.query(cls).filter(cls.codice_amazon == codice_amazon)


Base.metadata.create_all(engine)
