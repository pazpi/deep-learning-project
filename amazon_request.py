import bottlenose
import constant
from lxml import objectify
from urllib.error import HTTPError

# MaxQPS è il tempo fra una richiesta e l'altra, amazon impone un secondo di tempo fra una richiesta e l'altra
_amazon_api = bottlenose.Amazon(constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.AWS_ASSOCIATE_TAG,
                                MaxQPS=0.9,
                                Region='IT',
                                Parser=lambda text: objectify.fromstring(text))


def chunks(list_elements, chunk_size):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(list_elements), chunk_size):
        yield list_elements[i:i + chunk_size]


def api_request_product(codici_amazon, response_group='Large', merchant_id=''):
    """ Ottengo il file XML come una variabile.
    response_group indica la quantità di informazioni che vogliamo ottenere, Large contiene tutti i dati.
    Per maggiorni info:
        - http://s3.amazonaws.com/awsdocs/Associates/latest/prod-adv-api-qrc.pdf
    response_group: indica il livello di completezza che si vuole ottenere da parte di amazon, Large è il piu ciccione,
                 nel link [1] sono elencati tutti i campi esistenti. Es response_group='OfferFull, ItemAttributes'
                 I più utili sono (si possono anche aggregare assieme, separando con una virgola i campi voluti):
                   - Large             Completo, tutti i campi
                   - ItemAttributes    utile per rilevare il nome del prodotto da inserire nel database
                   - OfferFull         Trovare i vari prezzi di vendita
    """

    try:
        product = _amazon_api.ItemLookup(ItemId=codici_amazon, ResponseGroup=response_group, MerchantId=merchant_id)
        return product
    except HTTPError:
        # Questa parte non funziona e non so perchè
        return None


def get_element(product, path):
    """ Ottengo in maniera safe l'elemento in product che si trova nel percorso scelto

    :param product: Il codice XML formattato come variabile, contiene tutti i tag come oggetti nidificati
    :param path: Il percorso dove trovare l'elemento desiderato
    :return: L'elemento estratto dall'oggetto product
    """

    elements = path.split('.')
    if product is not None:
        for element in elements:
            try:
                product = getattr(product, element, None)
            except AttributeError:
                return None
            if product is None:
                return None
        return product.text
