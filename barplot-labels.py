# coding=utf-8
# Importare librerire
from sqlalchemy import and_
from sql_classes import Product, Session
import matplotlib
import matplotlib.pyplot as plt

# Creazione dizionario vuoto
prodotti_dict = {}

# Ottengo prodotti dal database
s = Session()
all_prod = s.query(Product).filter(and_(Product.image_url is not None, Product.product_type_name is not None)).all()
s.close()

# scorro gli elementi e li inserisco nel dizionario
for prod in all_prod:
    if prod.product_type_name is not None:
        if prod.product_type_name in prodotti_dict.keys():
            # se l'etichetta è presente nel dizionario aumento il valore di 1
            prodotti_dict[prod.product_type_name] += 1
        else:
            # se l'etichetta è nuova, la aggiungo al dizionario e imposto il valore a 1
            prodotti_dict[prod.product_type_name] = 1

prodotti_dict2 = {}
for key in prodotti_dict.keys():
    if prodotti_dict[key] > 20:
        prodotti_dict2[key] = prodotti_dict[key]

# bar plot dei valori
# conversione del dizionario in due array
label = list(prodotti_dict2.keys())
count = list(prodotti_dict2.values())
count_label = ["{} - {}".format(l, c) for l, c in zip(prodotti_dict2.values(), prodotti_dict2.keys())]
# figsize è in pollici, risoluzione ottenuta dai dpi
plt.figure(figsize=(8, 6), dpi=300)
plt.bar(label, count, width=.5)
# rovina il grafico ma non so perchè
plt.gcf().subplots_adjust(bottom=0.5)
plt.xlabel(count_label)
plt.xticks(rotation=90)

plt.show()
