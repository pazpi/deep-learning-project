import os
import requests
import multiprocessing
from io import BytesIO
from PIL import Image, ImageOps
from sql_classes import Session, Product
from sqlalchemy import func, desc, and_
from typing import List

NUMBER_OF_CLASS = 10
dataset_folder = './dataset-totar'


def save_prod(prods: List[Product], label: str):
    print('saving label {label}'.format(label=label))
    output_folder = os.path.join(dataset_folder, label)
    os.makedirs(output_folder, exist_ok=True)
    for prod in prods:
        response = requests.get(prod.image_url)

        img = Image.open(BytesIO(response.content))

        max_dim = max(img.size)
        border = (int((max_dim - img.size[0]) / 2),
                  int((max_dim - img.size[1]) / 2))

        img = ImageOps.expand(img, border=border, fill='white')
        img = ImageOps.fit(img, (max_dim, max_dim))
        img.save(os.path.join(output_folder, (prod.codice_amazon + '.jpg')))
    print('{label} done'.format(label=label))


def main():

    s = Session()

    best_label = s.query(Product.product_type_name).\
        group_by(Product.product_type_name).\
        order_by(desc(func.count(Product.product_type_name))).\
        limit(NUMBER_OF_CLASS)

    dataset = {}
    for label, in best_label:
        dataset[label] = s.query(Product).\
            filter(and_(Product.image_url != None, Product.product_type_name == label)).\
            all()

    s.close()

    # mutiprocessing
    jobs = []
    for key in dataset.keys():
        p = multiprocessing.Process(target=save_prod, args=(dataset[key], key))
        jobs.append(p)
        p.start()

    for job in jobs:
        job.join()

    print('done')


if __name__ == '__main__':
    main()
