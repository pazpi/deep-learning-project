import amazon_request
from sql_classes import Product, Session


def main():
    # sessione database
    session = Session()
    all_prod = session.query(Product).filter_by(image_url=None).all()
    all_prod.extend(session.query(Product).filter_by(product_type_name=None).all())
    n_prod = session.query(Product).count()
    session.close()

    # lista di blocchi di 10 codici
    generator_list_10_product = amazon_request.chunks(all_prod, 10)

    for list_product in generator_list_10_product:

        while True:
            print("id: {} / {}".format(list_product[0].id, n_prod))
            bulk_codici_amazon = [prod.codice_amazon for prod in list_product]
            bulk_codici_amazon = ','.join(bulk_codici_amazon)
            product = amazon_request.api_request_product(bulk_codici_amazon, response_group='Large')

            if product is not None:

                if hasattr(product, 'Items.Request.Errors'):
                    for err in product.Items.Request.Errors.Error:
                        print('Errore nella richiesta: {}'.format(err.Message))
                        # possibile eliminare il prodotto dal database

                for item in product.Items.Item:
                    asin = amazon_request.get_element(item, 'ASIN')
                    image_url = amazon_request.get_element(item, 'LargeImage.URL')
                    prod_cat = amazon_request.get_element(item, 'ItemAttributes.ProductTypeName')
                    session = Session()
                    session.query(Product). \
                        filter_by(codice_amazon=asin). \
                        update({Product.image_url: image_url,
                                Product.product_type_name: prod_cat})
                    session.commit()
                break
            else:
                print("Product is None\n{}".format(bulk_codici_amazon))


if __name__ == "__main__":
    main()
